FROM gradle:jdk AS builder

WORKDIR /app
ENV GRADLE_USER_HOME /app

COPY ./spring-boot-realworld-example-app-master .

# Optional gradle properties - e.g. for proxy configuration
COPY *gradle.properties .

RUN ./gradlew build

FROM adoptopenjdk:8-jre-hotspot

WORKDIR /app

ARG mysql_connector=mysql-connector-java-8.0.23
RUN curl -sL https://dev.mysql.com/get/Downloads/Connector-J/${mysql_connector}.tar.gz | tar -xz --strip-components=1 ${mysql_connector}/${mysql_connector}.jar

# Clear that property set in https://github.com/gothinkster/spring-boot-realworld-example-app/blob/master/src/main/resources/application.properties#L2
# It is stopping Spring to automatically choose a suitable driver for SPRING_DATASOURCE_URL
ENV SPRING_DATASOURCE_DRIVERCLASSNAME=""

EXPOSE 8080

COPY --from=builder /app/build/libs/app-0.0.1-SNAPSHOT.jar .

ENTRYPOINT java -cp "/app/*" org.springframework.boot.loader.JarLauncher
