# Realworld backend application

This is a [RealWorld](https://github.com/gothinkster/realworld-example-apps) backend application using a [copy](./spring-boot-realworld-example-app-master) of the [Spring Boot example implementation](https://github.com/gothinkster/spring-boot-realworld-example-app).

## Usage

The following shows how to build and run the app using a [Dockerfile](./Dockerfile). To run the application without docker, please see the [source docs](./spring-boot-realworld-example-app-master/README.md).

```bash
# Build the image:
docker build -t mybackend .

# Run it:
docker run -p 8080:8080 mybackend

# Check that it works:
curl http://localhost:8080/api/articles # Should output {"articles":[],"articlesCount":0}
```

## Configuration

Generally this app uses Spring Boot and its support for [externalized configuration](https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-external-config) which means in short you can adapt the [default configuration](https://github.com/gothinkster/spring-boot-realworld-example-app/blob/master/src/main/resources/application.properties) using various sources - in the following examples we'll simply use environment variables for this.

Please see the Spring Boot [chapter on relaxed binding](https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-external-config-relaxed-binding) for understanding how to map [property names](https://docs.spring.io/spring-boot/docs/current/reference/html/appendix-application-properties.html) to environment variable names.

### Database

By default the app uses a [sqlite database](https://github.com/gothinkster/spring-boot-realworld-example-app/blob/master/src/main/resources/application.properties#L1).
However it's possible to connect to a MySQL database instead starting with a different [datasource URL](https://docs.spring.io/spring-boot/docs/current/reference/html/appendix-application-properties.html#spring.datasource.url):

```bash
docker run -p 8080:8080 \
  -e SPRING_DATASOURCE_URL="jdbc:mysql://mydatabasehost:3306/mydatabase" \
  -e SPRING_DATASOURCE_USERNAME="MyDbUser" \
  -e SPRING_DATASOURCE_PASSWORD="MyDbPassword" \
  mybackend
```

(Note that the required mysql driver is provided in the [Dockerfile](./Dockerfile) - not in the actual sources)
